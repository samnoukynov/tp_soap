package endPoint;

import java.rmi.RemoteException;
import java.util.ArrayList;

import javax.jws.WebService;

import dao.LivreDAO;
import interfaces.ILivreEndPoint;
import metier.Livre;
import service.LivreService;

@WebService(endpointInterface = "interfaces.ILivreEndPoint")
public class LivreEndPoint implements ILivreEndPoint {
	
	private LivreDAO livreDAO ;
	private LivreService livreService ;
	
	public LivreEndPoint() {
		
		this.livreDAO = new LivreDAO() ;
		this.livreService = new LivreService() ;
	};

	public void createLivre(Integer isbn, String titre, Integer auteurId) {
		
		this.livreDAO.createLivre(isbn, titre, auteurId);
		
	}

	public void deleteLivre(Integer isbn) {
		this.livreDAO.deleteLivre(isbn);
		
	}

	public void updateTitreLivre(Integer isbn, String titre) {
		
		this.livreDAO.updateTitreLivre(isbn, titre);
		
	}

	public String readAllLivre() {
		return livreService.getAllAuteurString() ;
		
	}

	public void addAuteurToLivre(Integer isbn, Integer auteurId) {
		this.livreDAO.addAuteurToLivre(isbn, auteurId);
		
	}

	public void removeAuteurToLivre(Integer isbn, Integer auteurId) {
		this.livreDAO.removeAuteurToLivre(isbn, auteurId);
		
	}

}
