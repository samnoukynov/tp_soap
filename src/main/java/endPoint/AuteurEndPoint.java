package endPoint;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

import dao.AuteurDAO;
import interfaces.IEndPoint;
import metier.Auteur;
import service.AuteurService;

@WebService(endpointInterface = "interfaces.IEndPoint")
public class AuteurEndPoint implements IEndPoint {

	private AuteurDAO auteurDAO ;
	private AuteurService auteurService ;
	
	public AuteurEndPoint() {
		this.auteurDAO = new AuteurDAO();
		this.auteurService = new AuteurService();
	}
	

	public void createAuteur(Integer id, String nom ,String prenom) {

		this.auteurDAO.createAuteur(id, nom, prenom)  ;
		
	}

	public void deleteAuteur(int id) {
		
		this.auteurDAO.deleteAuteur(id);
		
	}

	public void updateAuteur(Integer id, String nom, String prenom) {
		
		this.auteurDAO.updateAuteur(id, nom, prenom);
		
	}
	
	public String readAllAuteur() {
		
		return auteurService.getAllAuteurString();
		
	}

	

}
