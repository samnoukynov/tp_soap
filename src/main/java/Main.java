import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import javax.xml.ws.Endpoint;
import endPoint.AuteurEndPoint;
import endPoint.LivreEndPoint;
import utilitaire.PropClass;

public class Main {
	
	

	public static void main(String[] args) {
		
		Connection conn = null; 
	    Statement stmt = null; 
	    Endpoint.publish("http://localhost:9998/ws/auteur", new AuteurEndPoint());
	    Endpoint.publish("http://localhost:9998/ws/livre", new LivreEndPoint());
	      try { 
	         // STEP 1: Register JDBC driver 
	         Class.forName(PropClass.JDBC_DRIVER); 
	             
	         //STEP 2: Open a connection 
	         System.out.println("Connecting to database..."); 
	         conn = DriverManager.getConnection(PropClass.DB_URL ,PropClass.USER,PropClass.PASS);  
	         
	         //STEP 3: Execute a query 
	         System.out.println("Creating table in given database..."); 
	         stmt = conn.createStatement(); 
	         String sql =  "\r\n" + 
	         		"DROP TABLE IF EXISTS AUTEUR; \r\n" + 
	         		"DROP TABLE IF EXISTS LIVRE;\r\n" + 
	         		"DROP TABLE IF EXISTS AUTEUR_LIVRE ;\r\n" + 
	         		"CREATE TABLE AUTEUR ( ID INT PRIMARY KEY , NOM VARCHAR(255) , PRENOM VARCHAR(255));\r\n" + 
	         		"INSERT INTO AUTEUR VALUES (1, 'jean', 'robert');\r\n" +
	         		"INSERT INTO AUTEUR VALUES (2, 'jule', 'vern');\r\n" +
	         		"INSERT INTO AUTEUR VALUES (3, 'emile', 'zola');\r\n" +
	         		"CREATE TABLE LIVRE( ISBN INT PRIMARY KEY , TITRE VARCHAR(255));\r\n" + 
	         		"INSERT INTO LIVRE VALUES (1, 'dragonball');" +
	         		"INSERT INTO LIVRE VALUES (2, 'watchmen');" +
	         		"INSERT INTO LIVRE VALUES (4, 'marsupilami');" +
	         		"INSERT INTO LIVRE VALUES (3, 'picsou');"+
	         		"CREATE TABLE AUTEUR_LIVRE ( AUTEUR_ID INT  , ISBN INT);\r\n" + 
	         		"INSERT INTO AUTEUR_LIVRE VALUES (1, 1);\r\n" + 
	         		"INSERT INTO AUTEUR_LIVRE VALUES (2, 2);\r\n" +
	         		"INSERT INTO AUTEUR_LIVRE VALUES (2, 4);\r\n" +
	         		"INSERT INTO AUTEUR_LIVRE VALUES (1, 3);\r\n" ; 
	         stmt.executeUpdate(sql);
	         System.out.println("Created table in given database..."); 
	         
	         // STEP 4: Clean-up environment 
	         stmt.close(); 
	         conn.close(); 
	      } catch(SQLException se) { 
	         //Handle errors for JDBC 
	         se.printStackTrace(); 
	      } catch(Exception e) { 
	         //Handle errors for Class.forName 
	         e.printStackTrace(); 
	      } finally { 
	         //finally block used to close resources 
	         try{ 
	            if(stmt!=null) stmt.close(); 
	         } catch(SQLException se2) { 
	         } // nothing we can do 
	         try { 
	            if(conn!=null) conn.close(); 
	         } catch(SQLException se){ 
	            se.printStackTrace(); 
	         } //end finally try 
	      } //end try 
	      System.out.println("Goodbye!");
	   } 
		
	}
