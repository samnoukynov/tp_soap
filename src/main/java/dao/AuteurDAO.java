package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import metier.Auteur;
import utilitaire.PropClass;

public class AuteurDAO {
	
	 
	public void createAuteur(Integer id, String nom, String prenom) {
		
		String sql = "INSERT INTO AUTEUR ( ID, NOM, PRENOM ) VALUES  ( "+ id + ", '"+ nom +"', '" +prenom+ "')" ;
		executeSql(sql);
	}
	public List<Auteur> readAllAuteur() {
		
		String sql = "SELECT * FROM AUTEUR " ;
		return executeSqlReadAllAuteur(sql);
	}
	public void updateAuteur(Integer id , String nom, String prenom) {
		String sql = "UPDATE AUTEUR SET NOM = '"+nom+ "', PRENOM = '" +prenom+ "' WHERE ID = '" +id+ "'" ;
		executeSql(sql);
	}
	public void deleteAuteur(Integer id) {
		String sql = "DELETE FROM AUTEUR WHERE ID = '" +id+ "';" ;
		sql += "DELETE FROM AUTEUR_LIVRE WHERE ID = '" +id+ "'" ;
		executeSql(sql);}
	
	private void executeSql(String query) {
		Connection conn = null; 
	    Statement stmt = null; 
	      try { 
	         // STEP 1: Register JDBC driver 
	         Class.forName(PropClass.JDBC_DRIVER); 
	             
	         //STEP 2: Open a connection 
	         System.out.println("Connecting to database..."); 
	         conn = DriverManager.getConnection(PropClass.DB_URL,PropClass.USER,PropClass.PASS);  
	         
	         //STEP 3: Execute a query 
	         System.out.println("execute query in database..."); 
	         stmt = conn.createStatement();   
	         stmt.executeUpdate(query);
	         System.out.println("query executed !"); 
	         
	         // STEP 4: Clean-up environment 
	         stmt.close(); 
	         conn.close(); 
	      } catch(SQLException se) { 
	         //Handle errors for JDBC 
	         se.printStackTrace(); 
	      } catch(Exception e) { 
	         //Handle errors for Class.forName 
	         e.printStackTrace(); 
	      } finally { 
	         //finally block used to close resources 
	         try{ 
	            if(stmt!=null) stmt.close(); 
	         } catch(SQLException se2) { 
	         } // nothing we can do 
	         try { 
	            if(conn!=null) conn.close(); 
	         } catch(SQLException se){ 
	            se.printStackTrace(); 
	         } //end finally try 
	      } //end try 
	}
	
	private List<Auteur> executeSqlReadAllAuteur(String query) {
		Connection conn = null; 
	    Statement stmt = null; 
	    List<Auteur> listAuteur = new ArrayList() ;
	      try { 
	         // STEP 1: Register JDBC driver 
	         Class.forName(PropClass.JDBC_DRIVER); 
	             
	         //STEP 2: Open a connection 
	         System.out.println("Connecting to database..."); 
	         conn = DriverManager.getConnection(PropClass.DB_URL,PropClass.USER,PropClass.PASS);  
	         
	         //STEP 3: Execute a query 
	         System.out.println("execute query in database..."); 
	         stmt = conn.createStatement();   
	         //stmt.executeUpdate(query);
	         ResultSet rs = stmt.executeQuery(query);
	         Auteur auteur = null;
	         String id = null;
             String nom = null;
             String prenom = null;
	         while (rs.next()) {
	        	 id = rs.getString("ID");
	             nom = rs.getString("NOM");
	             prenom = rs.getString("PRENOM");
	             System.out.println(id);
	             System.out.println(nom);
	             System.out.println(prenom);
	             auteur = new Auteur(Integer.parseInt(id),nom,prenom);
	             listAuteur.add(auteur);
	         }
	         System.out.println("query executed !"); 
	         
	         // STEP 4: Clean-up environment 
	         stmt.close(); 
	         conn.close(); 
	      } catch(SQLException se) { 
	         //Handle errors for JDBC 
	         se.printStackTrace(); 
	      } catch(Exception e) { 
	         //Handle errors for Class.forName 
	         e.printStackTrace(); 
	      } finally { 
	         //finally block used to close resources 
	         try{ 
	            if(stmt!=null) stmt.close(); 
	         } catch(SQLException se2) { 
	         } // nothing we can do 
	         try { 
	            if(conn!=null) conn.close(); 
	         } catch(SQLException se){ 
	            se.printStackTrace(); 
	         } //end finally try 
	      } 
	      return listAuteur;
	}	
}
