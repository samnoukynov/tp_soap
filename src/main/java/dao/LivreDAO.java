package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import metier.Auteur;
import metier.Livre;
import utilitaire.PropClass;

public class LivreDAO {
	
public void createLivre(Integer isbn, String titre, Integer auteurId ) {
		
		String sql = "INSERT INTO LIVRE ( ISBN, TITRE ) VALUES  ( "+ isbn + ", '"+ titre +"');" ;
		sql += "INSERT INTO AUTEUR_LIVRE ( AUTEUR_ID, ISBN ) VALUES  ( "+ auteurId + ", '"+ isbn +"') ";
		executeSql(sql);
	}
	public ArrayList<Livre> readAllLivre() {
		
		String sql = "SELECT l.ISBN , l.TITRE, a.NOM as NOM, a.PRENOM as PRENOM  FROM LIVRE l INNER JOIN AUTEUR_LIVRE al on l.ISBN = al.ISBN "
				+ " INNER JOIN AUTEUR a on a.ID = al.AUTEUR_ID " ;
		return executeSqlReadAllLivre(sql);
	}
	public void updateTitreLivre(Integer isbn , String titre) {
		String sql = "UPDATE LIVRE SET TITRE = '" +titre+ "' WHERE ISBN = " +isbn+ "" ;
		executeSql(sql);
	}
	
	public void addAuteurToLivre(Integer isbn , Integer id) {
		String sql = "INSERT INTO AUTEUR_LIVRE ( AUTEUR_ID, ISBN ) VALUES  ( "+ id + ", '" +isbn+ "');"; 
		executeSql(sql);
	}
	
	public void removeAuteurToLivre(Integer isbn , Integer id) {
		String sql = " DELETE FROM AUTEUR_LIVRE WHERE  AUTEUR_ID = " +id+ " AND IBSN = " +isbn+  ""; 
		executeSql(sql);
	}
	
	public void deleteLivre(Integer isbn) {
		String sql = "DELETE FROM LIVRE WHERE ISBN = " +isbn+ " ;" ;
		sql += "DELETE FROM AUTEUR_LIVRE WHERE ISBN = " +isbn+ " ";
		executeSql(sql);}
	
	private void executeSql(String query) {
		Connection conn = null; 
	    Statement stmt = null; 
	      try { 
	         // STEP 1: Register JDBC driver 
	         Class.forName(PropClass.JDBC_DRIVER); 
	             
	         //STEP 2: Open a connection 
	         System.out.println("Connecting to database..."); 
	         conn = DriverManager.getConnection(PropClass.DB_URL,PropClass.USER,PropClass.PASS);  
	         
	         //STEP 3: Execute a query 
	         System.out.println("execute query in database..."); 
	         stmt = conn.createStatement();   
	         stmt.executeUpdate(query);
	         System.out.println("query executed !"); 
	         
	         // STEP 4: Clean-up environment 
	         stmt.close(); 
	         conn.close(); 
	      } catch(SQLException se) { 
	         //Handle errors for JDBC 
	         se.printStackTrace(); 
	      } catch(Exception e) { 
	         //Handle errors for Class.forName 
	         e.printStackTrace(); 
	      } finally { 
	         //finally block used to close resources 
	         try{ 
	            if(stmt!=null) stmt.close(); 
	         } catch(SQLException se2) { 
	         } // nothing we can do 
	         try { 
	            if(conn!=null) conn.close(); 
	         } catch(SQLException se){ 
	            se.printStackTrace(); 
	         } //end finally try 
	      } //end try 
	}
	
	private ArrayList<Livre> executeSqlReadAllLivre(String query) {
		Connection conn = null; 
	    Statement stmt = null; 
	    ArrayList<Livre> listAuteur = new ArrayList() ;
	      try { 
	         // STEP 1: Register JDBC driver 
	         Class.forName(PropClass.JDBC_DRIVER); 
	             
	         //STEP 2: Open a connection 
	         System.out.println("Connecting to database..."); 
	         conn = DriverManager.getConnection(PropClass.DB_URL,PropClass.USER,PropClass.PASS);  
	         
	         //STEP 3: Execute a query 
	         System.out.println("execute query in database..."); 
	         stmt = conn.createStatement();   
	         //stmt.executeUpdate(query);
	         ResultSet rs = stmt.executeQuery(query);
	         Livre livre = null;
	         Integer isbn = null;
             String titre = null;
             String auteur = null;
             String nom = null;
             String prenom = null;
	         while (rs.next()) {
	        	 isbn = rs.getInt("ISBN");
	        	 titre = rs.getString("TITRE");
	        	 //auteur = rs.getString("AUTEUR");
	        	 nom = rs.getString("NOM");
	        	 prenom = rs.getString("PRENOM");
	             System.out.println(isbn);
	             System.out.println(titre);
	             //System.out.println(prenom);
	             livre = new Livre(isbn,titre);
	             listAuteur.add(livre);
	         }
	         System.out.println("query executed !"); 
	         
	         // STEP 4: Clean-up environment 
	         stmt.close(); 
	         conn.close(); 
	      } catch(SQLException se) { 
	         //Handle errors for JDBC 
	         se.printStackTrace(); 
	      } catch(Exception e) { 
	         //Handle errors for Class.forName 
	         e.printStackTrace(); 
	      } finally { 
	         //finally block used to close resources 
	         try{ 
	            if(stmt!=null) stmt.close(); 
	         } catch(SQLException se2) { 
	         } // nothing we can do 
	         try { 
	            if(conn!=null) conn.close(); 
	         } catch(SQLException se){ 
	            se.printStackTrace(); 
	         } //end finally try 
	      } 
	      return listAuteur;
	}	

}
