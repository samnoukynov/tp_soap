package metier;

public class Livre {

	private Integer isbn ;
	private String titre ;
	
	public Livre(Integer isbn, String titre) {
		super();
		this.isbn = isbn;
		this.titre = titre;
	}
	
	public Integer getIsbn() {
		return isbn;
	}
	public void setIsbn(Integer isbn) {
		this.isbn = isbn;
	}
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	
	
	
	
}
