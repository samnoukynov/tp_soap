package interfaces;


import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import metier.Auteur;

@WebService
@SOAPBinding(style = Style.RPC , use = Use.LITERAL)
public interface IEndPoint {

	@WebMethod 
	public void createAuteur(Integer id, String nom ,String prenom);
	
	@WebMethod
	public void deleteAuteur(int id);
	
	@WebMethod
	public void updateAuteur(Integer id , String nom, String prenom);
	
	@WebMethod
	public String readAllAuteur();
	
}
