/**
 * ILivreEndPoint.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package interfaces;

import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;

import metier.Livre;

@WebService
@SOAPBinding(style = Style.RPC , use = Use.LITERAL)
public interface ILivreEndPoint extends java.rmi.Remote {
    public void deleteLivre(Integer arg0) throws java.rmi.RemoteException;
    public void createLivre(Integer arg0, String arg1, Integer arg2) throws java.rmi.RemoteException;
    public String readAllLivre() throws java.rmi.RemoteException;
    public void updateTitreLivre(Integer arg0, String arg1) throws java.rmi.RemoteException;
    public void addAuteurToLivre(Integer isbn, Integer auteurId);
    public void removeAuteurToLivre(Integer isbn, Integer auteurId);
}
