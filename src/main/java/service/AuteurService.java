package service;

import java.util.List;

import dao.AuteurDAO;
import metier.Auteur;

public class AuteurService {
	
	public String getAllAuteurString() {
		
		AuteurDAO auteurDAO = new AuteurDAO();
		List<Auteur> listAuteur = auteurDAO.readAllAuteur();
		String stringListAuteur = "" ;
		for (Auteur auteur : listAuteur){
			
			stringListAuteur += "| "+ auteur.getPrenom() + " " + auteur.getNom() + " |" ;

			}	
		return stringListAuteur ;
	}

}
