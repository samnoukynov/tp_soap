package service;

import java.util.List;

import dao.AuteurDAO;
import dao.LivreDAO;
import metier.Auteur;
import metier.Livre;

public class LivreService {
	
public String getAllAuteurString() {
		
		LivreDAO livreDAO = new LivreDAO();
		List<Livre> listlivre = livreDAO.readAllLivre();
		String stringListLivre = "" ;
		for (Livre livre : listlivre){
			
			stringListLivre += "| "+ livre.getTitre() + " " + livre.getIsbn() + " |" ;

			}	
		return stringListLivre ;

}
}
